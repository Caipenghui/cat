/* 
 * Copyright 2018 Caipenghui
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "types.h"
#include "stat.h"
#include "user.h"

char buf[999];

void cat(int fd)
{
	int n;
	
	while((n = read(fd, buf, sizeof(buf))) > 0) {
                if (write(1, buf, n) != n) {
                        printf(1, "cat: write error\n");
                        exit();
                }
	}	
	if(n < 0) {
		printf(1, "cat: read error\n");
		exit();
	}
}

int main(int argc, char *argv[])
{
	int fd, i;
	
	if(argc <= 1) {
		cat(0);
		exit();
	}
	for(i = 1; i < argc; i++) {
		if((fd = open(argv[i], 0)) < 0) {
			printf(1, "cat: cannot open %s\n", argv[i]);
			exit();
                }
        }
	cat(fd);
	close(fd);
	exit();
}
